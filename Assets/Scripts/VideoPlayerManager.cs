﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine.UI;
using Extensions;
using UnityEngine.Events;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
[RequireComponent(typeof(VideoPlayer),typeof(RawImage),typeof(AudioSource))]
public class VideoPlayerManager : MonoBehaviour
{

    #region Fields and Properties

    [SerializeField]
    VideoPlayer videoPlayer;

    [SerializeField]
    AudioSource audioSource;

    [SerializeField]
    RawImage rawImage;

    [SerializeField]
    float videoPlayerMaskDuration = 2.5f;

    [SerializeField]
    bool deactivateOnFinish;

    public UnityEvent onVideoEnded;

    public float videoDisplayAlpha
    {
        get { return rawImage.color.a; }
        set { rawImage.color = rawImage.color.WithAlpha(value); }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        if(videoPlayer == null)
        {
            videoPlayer = GetComponent<VideoPlayer>();
        }

        if (rawImage == null)
        {
            rawImage = GetComponent<RawImage>();
        }

        if (audioSource == null)
        {
            audioSource = GetComponent<AudioSource>();
        }
        videoPlayer.loopPointReached +=
                           (source) =>
                           {
                               onVideoEnded.Invoke();
                               DOTween.To(() => videoDisplayAlpha,
                                          alpha => videoDisplayAlpha = alpha,
                                          0,
                                          videoPlayerMaskDuration);
                                          if (deactivateOnFinish)
                                            videoPlayer.gameObject.SetActive(false);
                           };

        videoPlayer.prepareCompleted +=
                       (source) =>
                       {
                           DOTween.To(() => videoDisplayAlpha,
                                      alpha => videoDisplayAlpha = alpha,
                                      1,
                                      videoPlayerMaskDuration);
                       };
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

    #endregion

    #region Methods

	public void Play()
    {
        videoPlayer.Play();
    }

    public void Pause()
    {
        videoPlayer.Pause();
    }

    public void CloseVideoPlayer()
    {
        
    }

    #endregion
}
