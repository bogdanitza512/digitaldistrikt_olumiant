﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// 
/// </summary>
public class AdditionalButtonManager : MonoBehaviour
{

    #region Fields and Properties

	

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

    #endregion

    #region Methods

	public void OnReferenceButton_Pressed()
    {
        if (RCPSlide.Instance != null)
            RCPSlide.Hide();
        ReferenceSlide.Show();
        NavbarManager.Instance.TryTransitionTo(NavbarManager.NavbarState.Closed);
    }

    public void OnRCPButton_Pressed()
    {
        if (ReferenceSlide.Instance != null)
            ReferenceSlide.Hide();
        RCPSlide.Show();
        NavbarManager.Instance.TryTransitionTo(NavbarManager.NavbarState.Closed);
    }

    public void OnRestartButton_Pressed()
    {
        // Application.LoadLevel(Application.loadedLevel);
        var sceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneName);
    }

    public void OnBackButton_Pressed()
    {
        SlideManager.Instance.OnBackButton_Pressed();
        NavbarManager.Instance.TryTransitionTo(NavbarManager.NavbarState.Closed);
    }

    #endregion
}
