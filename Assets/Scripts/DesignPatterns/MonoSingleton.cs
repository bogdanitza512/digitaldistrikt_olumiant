﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{

    #region Fields and Properties

    private static T _instance;

    private static object _lock = new object();

    private static bool applicationIsQuitting = false;

    public static T Instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                print("[MonoSingleton] Instance '" + typeof(T) +
                    "' already destroyed on application quit." +
                    " Won't create again - returning null.");
                return null;
            }

            lock (_lock)
            {
                if (_instance == null)
                {
                    var singletonsFound = FindObjectsOfType(typeof(T));
                    _instance = singletonsFound[0] as T;
                    if(singletonsFound.Length > 1)
                    {
                        print("[MonoSingleton] Something went really wrong " +
                              " - there should never be more than 1 singleton!" +
                              " Reopening the scene might fix it.");
                        return _instance;
                    }
                    /*  
                    _instance = (T)FindObjectOfType(typeof(T));

                    if (FindObjectsOfType(typeof(T)).Length > 1)
                    {
                        Debug.LogError("[Singleton] Something went really wrong " +
                            " - there should never be more than 1 singleton!" +
                            " Reopening the scene might fix it.");
                        return _instance;
                    }
                    */

                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject();
                        _instance = singleton.AddComponent<T>();
                        singleton.name = "[MonoSingleton] " + typeof(T).ToString();

                        DontDestroyOnLoad(singleton);

                        print("[MonoSingleton] An instance of " + typeof(T) +
                            " is needed in the scene, so '" + singleton +
                            "' was created with DontDestroyOnLoad.");
                    }
                    else
                    {
                        print("[MonoSingleton] Using instance already created: " +
                            _instance.gameObject.name);
                    }
                }

                return _instance;
            }
        }
    }


    #endregion

    #region Unity Messages

    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    /// it will create a buggy ghost object that will stay on the Editor scene
    /// even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    public void OnDestroy()
    {
        print("{0} is being destroyed.".FormatWith(typeof(T).Name));
        applicationIsQuitting = true;
    }

    #endregion

    #region Methods

	

    #endregion

}
