﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class ClinicalStudiesSlide : SimpleSlide<ClinicalStudiesSlide>
{

    #region Fields and Properties

	

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }



	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
    {
	
    }

	#endregion

	#region Methods

	public override void OnOpen()
	{
        base.OnOpen();
        ClinicalStudiesStudyProgramsSlide.Show();
	}

	public void OnStudyProgramButton_Pressed()
    {
        
        if ( ClinicalStudiesEfficacySlide.Instance != null)
            ClinicalStudiesEfficacySlide.Hide();
        ClinicalStudiesStudyProgramsSlide.Show();
    }

    public void OnEfficacyButton_Pressed()
    {
        if (ClinicalStudiesStudyProgramsSlide.Instance != null)
            ClinicalStudiesStudyProgramsSlide.Hide();
        ClinicalStudiesEfficacySlide.Show();
    }

    #endregion
}
