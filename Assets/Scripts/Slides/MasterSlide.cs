﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

/// <summary>
/// 
/// </summary>
public class MasterSlide : SerializedMonoSingleton<MasterSlide>
{

    #region Fields and Properties

    [SerializeField]
    Animator animator;

    public enum TabsState
    {
        Idle,
        SwingLeft,
        SwingLeftRight
    }
    [SerializeField]
    TabsState lastTabsState = TabsState.Idle;

    [SerializeField]
    Dictionary<TabsState, string> tabsTriggerMap = new Dictionary<TabsState, string>();

    [SerializeField]
    VideoPlayerManager videoPlayer;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

    public void OnLillyPlayButton_Pressed()
    {
        var succes = TryPerformTransitionTo(TabsState.SwingLeftRight);
        if (succes)
        {
            videoPlayer.gameObject.SetActive(true);
            videoPlayer.Play();
        }
        else
        {
            videoPlayer.Pause();
            TryPerformTransitionTo(TabsState.Idle);
        }
    }

    public void OnOlumiantPlayButton_Pressed()
    {
        var succes = TryPerformTransitionTo(TabsState.SwingLeft);
        if (!succes)
        {
            TryPerformTransitionTo(TabsState.Idle);
        }
        else { HomeSlide.Show(); }
    }

    public bool TryPerformTransitionTo(TabsState newState)
    {
        if (lastTabsState != newState)
        {
            animator.SetTrigger(tabsTriggerMap[newState]);
            lastTabsState = newState;
            return true;
        }
        return false;
    }

    #endregion

}
