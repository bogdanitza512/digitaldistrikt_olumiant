﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class SafetyMalignancySlide : SimpleSlide<SafetyMalignancySlide>
{

    #region Fields and Properties

	

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        SafetyMalignancyLongTermSlide.Show();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

	#endregion

	#region Methods

	public void OnLongTermButton_Pressed()
    {
        
        if (SafetyMalignancyPlaceboSlide.Instance != null)
            SafetyMalignancyPlaceboSlide.Hide();
        SafetyMalignancyLongTermSlide.Show();
    }

    public void OnPlaceboButton_Pressed()
    {
        if (SafetyMalignancyLongTermSlide.Instance != null)
            SafetyMalignancyLongTermSlide.Hide();
        SafetyMalignancyPlaceboSlide.Show();
    }

    #endregion
}
