﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class EfficacyInhibitionSlide : SimpleSlide<EfficacyInhibitionSlide>
{

    #region Fields and Properties

    [SerializeField]
    MasterSwitch masterSwitch;

    [SerializeField]
    Animator graphAnimator;

    [SerializeField]
    string openWeek24TriggerName;

    [SerializeField]
    string openYear01TriggerName;

    [SerializeField]
    string openYear02TriggerName;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
	    
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

    #endregion

    #region Methods

	public void OnWeek24Button_Pressed()
    {
        graphAnimator.SetTrigger(openWeek24TriggerName);
    }

    public void OnYear1Button_Pressed()
    {
        graphAnimator.SetTrigger(openYear01TriggerName);
    }

    public void OnYear2Button_Pressed()
    {
        graphAnimator.SetTrigger(openYear02TriggerName);
    }

	public override void OnOpen()
	{
        //masterSwitch.ChangeStateTo(0);
        graphAnimator.SetTrigger(openWeek24TriggerName);
	}

	#endregion
}
