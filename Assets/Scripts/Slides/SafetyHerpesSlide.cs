﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class SafetyHerpesSlide : SimpleSlide<SafetyHerpesSlide>
{

    #region Fields and Properties

	

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        SafetyHerpesLongTermSlide.Show();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

    #endregion

    #region Methods

    public void OnLongTermButton_Pressed()
    {

        if (SafetyHerpesPlaceboSlide.Instance != null)
            SafetyHerpesPlaceboSlide.Hide();
        SafetyHerpesLongTermSlide.Show();
    }

    public void OnPlaceboButton_Pressed()
    {
        if (SafetyHerpesLongTermSlide.Instance != null)
            SafetyHerpesLongTermSlide.Hide();
        SafetyHerpesPlaceboSlide.Show();
    }

    #endregion
}
