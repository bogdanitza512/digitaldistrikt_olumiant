﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class SafetySlide : SimpleSlide<SafetySlide>
{

    #region Fields and Properties

	

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

    #endregion

    #region Methods

	public void OnMalignancyButton_Pressed()
    {
        SafetyMalignancySlide.Show();
    }

    public void OnInfectionsButton_Pressed()
    {
        SafetyInfectionsSlide.Show();
    }

    public void OnHerpesButton_Pressed()
    {
        SafetyHerpesSlide.Show();
    }

    #endregion
}
