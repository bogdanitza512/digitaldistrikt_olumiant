﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class MOASlide : SimpleSlide<MOASlide>
{

    #region Fields and Properties

    [SerializeField]
    Animator diagramAnimator;

    [SerializeField]
    string openDiagramTriggerName;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

    #endregion

    #region Methods

    public override void OnOpen()
    {
        base.OnOpen();
        diagramAnimator.SetTrigger(openDiagramTriggerName);
    }

    #endregion
}
