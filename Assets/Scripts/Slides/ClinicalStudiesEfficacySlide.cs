﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class ClinicalStudiesEfficacySlide : SimpleSlide<ClinicalStudiesEfficacySlide>
{

    #region Fields and Properties

    [SerializeField]
    Animator graphAnimator;

    [SerializeField]
    string openGraphTriggerName;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

    #endregion

    #region Methods

    public override void OnBackPressed()
    {
        base.OnBackPressed();
        ClinicalStudiesSlide.Close();
    }

	public override void OnOpen()
	{
        base.OnOpen();
        graphAnimator.SetTrigger(openGraphTriggerName);
	}

	#endregion
}
