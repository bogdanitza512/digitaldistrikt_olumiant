﻿using Extensions;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// 
/// </summary>
public class NavBarManager : MonoBehaviour
{

    #region Fields and Properties

    public RectTransform navBar;

    [SerializeField]
    float openCloseDuration = 2.5f;

    [SerializeField]
    Ease easeFunction = Ease.InOutCubic;

    public float navBarOpenX;

    public float navBarClosedX;

    public enum NavBarState { Opened, Transition, Closed }

    public NavBarState drawerState = NavBarState.Closed;

    [ButtonGroup]
    [Button("Set Opened X")]
    void SetOpenedX()
    {
        if(navBar != null)
        {
            navBarOpenX = navBar.anchoredPosition.x;
        }
    }
    [ButtonGroup]
    [Button("Set Closed X")]
    void SetClosedX()
    {
        if (navBar != null)
        {
            navBarClosedX = navBar.anchoredPosition.x;
        }
    }

    [Button("Toggle NavBar Drawer States",ButtonSizes.Medium)]
    void ToggleNavBar()
    {
        if(drawerState == NavBarState.Closed)
        {
            navBar.anchoredPosition = navBar.anchoredPosition.WithX(navBarOpenX);
            drawerState = NavBarState.Opened;
        }
        else
        {
            navBar.anchoredPosition = navBar.anchoredPosition.WithX(navBarClosedX);
            drawerState = NavBarState.Closed;
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

    #endregion

    #region Methods

    public void OpenNavBar ()
    {

        navBar.DOAnchorPosX(navBarOpenX, openCloseDuration)
              .SetEase(easeFunction)
              .OnComplete(() =>
                {
                    print("[NavBar]: Drawer opened...");
                    drawerState = NavBarState.Opened;
                });
        drawerState = NavBarState.Transition;
    }

    public void CloseNavBar ()
    {
        
        navBar.DOAnchorPosX(navBarClosedX, openCloseDuration)
              .SetEase(easeFunction)
              .OnComplete(() => 
                {
                    print("[NavBar]: Drawer closed...");
                    drawerState = NavBarState.Closed;
                });
        drawerState = NavBarState.Transition;
    }

    public enum NavBarOption 
    {
        Home = 0,
        PatientProfile = 1,
        TreatmentParadigm = 2,
        MOA = 3,
        Efficacy = 4, 
        PROs = 5, 
        ClinicalTrial = 6,
        Dosing = 7, 
        Safety = 8, 
        Summary = 9
    }
	
    public Dictionary<NavBarOption, GameObject> optionMap;

    public void OnNavBarOptionClicked (NavBarOption option)
    {
        
    }

    #endregion
}