﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using DG.Tweening;
using TMPro;
using Extensions;
using UnityEngine.Events;

/// <summary>
/// 
/// </summary>
public class MasterSwitch : MonoBehaviour
{

    #region Fields and Properties

    [SerializeField]
    Image knob;

    [SerializeField]
    float stateChangeDuration;

    [SerializeField]
    Color activeTextColor;

    [SerializeField]
    Color inactiveTextColor;

    [SerializeField]
    List<SwitchState> switchStateList = new List<SwitchState>();

    [SerializeField]
    UnityEvent onStateChange;

    int lastStateIndex = 0;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    void ToggleStates()
    {
        var stateIndex = lastStateIndex;
        stateIndex = lastStateIndex + 1 == switchStateList.Count ? 0 : lastStateIndex + 1;
        knob.rectTransform.anchoredPosition = switchStateList[stateIndex].pos;
        switchStateList[stateIndex].color = activeTextColor;
        switchStateList[lastStateIndex].color = inactiveTextColor;
        lastStateIndex = stateIndex;
    }


    public void ChangeStateTo(int stateIndex)
    {
        if (lastStateIndex != stateIndex)
        {
            DOTween.Sequence()
                   .Append(knob.rectTransform.DOAnchorPos(switchStateList[stateIndex].pos, stateChangeDuration))
                   .Join(DOTween.To(() => switchStateList[stateIndex].color,
                                    color => switchStateList[stateIndex].color = color,
                                    activeTextColor,
                                    stateChangeDuration))
                   .Join(DOTween.To(() => switchStateList[lastStateIndex].color,
                                    color => switchStateList[lastStateIndex].color = color,
                                    inactiveTextColor,
                                    stateChangeDuration))
                   .OnComplete(() => lastStateIndex = stateIndex);
        }
    }

    #endregion
}
