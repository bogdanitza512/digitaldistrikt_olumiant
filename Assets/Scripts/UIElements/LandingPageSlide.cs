﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine.UI;
using DG.Tweening;
using Extensions;
using Sirenix.OdinInspector;

/// <summary>
/// 
/// </summary>
[RequireComponent(typeof(Animator))]
public class LandingPageSlide : SerializedMonoBehaviour
{

    #region Fields and Properties

    [SerializeField]
    Animator anim;

    public enum LandingPageState{ Idle, SwingedLeft, SwingedRightLeft }
    [SerializeField]
    LandingPageState lastState = LandingPageState.Idle;
    [SerializeField]
    Dictionary<LandingPageState, string> landingPageTriggerNameMap;

    [SerializeField]
    VideoPlayer videoPlayer;
    [SerializeField]
    RawImage rawImage;
    [SerializeField]
    float videoPlayerMaskDuration = 2.5f;

    [SerializeField]
    NavbarManager navbar;

    float videoDisplayAlpha
    {
        get{ return rawImage.color.a; }
        set{ rawImage.color = rawImage.color.WithAlpha(value); }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        if (anim == null)
        {
            anim = GetComponent<Animator>();
        }

        if(videoPlayer == null)
        {
            videoPlayer = GetComponentInChildren<VideoPlayer>(includeInactive: true);
        }
        if(rawImage == null)
        {
            rawImage = videoPlayer.gameObject.GetComponent<RawImage>();
        }

        videoPlayer.loopPointReached +=
            (source) =>
        {
            TryPerformTransitionTo(LandingPageState.Idle);
            DOTween.To(() => videoDisplayAlpha,
                       alpha => videoDisplayAlpha = alpha,
                       0,
                       videoPlayerMaskDuration);
            videoPlayer.gameObject.SetActive(false);
        };

        videoPlayer.prepareCompleted +=
            (source) =>
        {
            DOTween.To(() => videoDisplayAlpha,
                       alpha => videoDisplayAlpha = alpha,
                       1,
                       videoPlayerMaskDuration);
        };
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

	public void OnLillyPlayButton_Pressed()
    {
        var succes = TryPerformTransitionTo(LandingPageState.SwingedRightLeft);
        if(succes)
        {
            videoPlayer.gameObject.SetActive(true);
            videoPlayer.Play();
        }
    }

    public void OnOlumiantPlayButton_Pressed()
    {
        var succes = TryPerformTransitionTo(LandingPageState.SwingedLeft);
        if(succes)
        {
            videoPlayer.gameObject.SetActive(false);
            DOTween.To(() => videoDisplayAlpha,
                       alpha => videoDisplayAlpha = alpha,
                       0,
                       videoPlayerMaskDuration);
            HomeSlide.Show();
        }
    }

    public bool TryPerformTransitionTo(LandingPageState newState)
    {
        if (lastState != newState)
        {
            anim.SetTrigger(landingPageTriggerNameMap[newState]);
            lastState = newState;
            return true;
        }
        return false;
    }

    public void TriggerNavbar()
    {
        navbar.gameObject.SetActive(true);
        print("Navbar Activated");
        navbar.TryTransitionTo(NavbarManager.NavbarState.Opened);
    }

    #endregion
}
