﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class CallToActionManager : MonoBehaviour
{
    void HandleTweenCallback()
    {
    }


    #region Fields and Properties

    [SerializeField]
    CanvasGroup canvasGroup;

    [SerializeField]
    Image blueSlider;

    [SerializeField]
    Image orangeSlider;

    [SerializeField]
    Button playButton;

    [SerializeField]
    Image outerGlow;

    [SerializeField]
    float sliderDelta = 1.0f;

    [SerializeField]
    float holdTime = 10.0f;

    [SerializeField]
    Sequence fillSequence;

    [SerializeField]
    Color victoryColor;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

    public void OnHandButton_Pressed()
    {
        canvasGroup.DOFade(0, 2.5f)
                   .OnComplete(() =>
                   { gameObject.SetActive(false); });
    }

    public void OnPointerDown()
    {
       // blueSlider.fillAmount = 0;
       // orangeSlider.fillAmount = 0;

        print(holdTime - holdTime * blueSlider.fillAmount);

        fillSequence = DOTween.Sequence()
                              .Append(blueSlider.DOFillAmount(1, holdTime - holdTime * blueSlider.fillAmount)
                                                .SetEase(Ease.Linear))
                              .Join(orangeSlider.DOFillAmount(1, holdTime - holdTime * blueSlider.fillAmount - sliderDelta)
                                                .SetEase(Ease.Linear))
                              .OnComplete(() => 
                               {
                                    outerGlow.DOFade(0.75f, 2.0f)
                                             .SetLoops(-1,LoopType.Yoyo);
                               });
    }

    public void OnPointerUp()
    {
        if (fillSequence.IsPlaying())
            fillSequence.PlayBackwards();
        else
            canvasGroup.DOFade(0, 2.0f)
                       .OnComplete(() => gameObject.SetActive(false));
    }

    #endregion
}
